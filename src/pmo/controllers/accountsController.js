const log4js = require("log4js");
const logger = log4js.getLogger("accountsController");
const jsonwebtoken = require('../../utils/jsonwebtoken');
const https = require('https');
const util = require('../../utils/util');
const entitlementsController = require('../controllers/entitlementsController')
const constants = require('../../utils/constants');

var methods = {};
methods.getAllAccounts = (req, res) =>
{
    const token = jsonwebtoken.getTokenData(req);

    if (token == null || token.length === 0)
    {
        logger.error("getAllAccounts - "+constants.TOKEN_ABSENT);
        return res.status(403).json({"message": constants.TOKEN_ABSENT});
    }

    const options = util.httpOption(token, 'GET', constants.ASE_CONSOLEUSERS);

    try {
        const req1 = https.request(options,  (res1) => {
            if (res1.statusCode !== 200)
            {
                logger.error("getAllAccounts - " + res1.statusCode + " : "+res1.statusMessage);
                return res.status(res1.statusCode).json({"message": res1.statusMessage});                
            }
            res1.on('data', async (d) => {
                var data = [];
                var accounts = JSON.parse(d);
                var entitlements = await entitlementsController.getEntitlementsMap(token);

                accounts.forEach(account => {
                    if (account[constants.ISGROUP] !== true){
                        data.push(methods.getJSONAccountObject(entitlements, account));
                    }
                });


                return res.status(200).json(data);
            }).on('error', (error) => {
                    logger.error("getAllAccounts - " + error);
                    return res.status(400).json({"message": constants.WRONG_INPUT});
                }); 
        });
    
        req1.end();        
    } catch (error) {
        logger.error("getAllAccounts - " + error);
        res.status(500).json({"message": error});
    }
}

methods.getAccount = (req, res) =>
{
    const token = jsonwebtoken.getTokenData(req);

    if (token == null || token.length === 0)
    {
        logger.error("getAccount - "+constants.TOKEN_ABSENT);
        return res.status(403).json({"message": constants.TOKEN_ABSENT});
    }

    const options = util.httpOption(token, 'GET', constants.ASE_CONSOLEUSERS+req.params.accountid);

    try {
        const req1 = https.request(options,  (res1) => {
            if (res1.statusCode !== 200)
            {
                logger.error("getAccount - " + res1.statusCode + " : "+res1.statusMessage);
                return res.status(res1.statusCode).json({"message": res1.statusMessage});
            }
            res1.on('data', async (d) => {
                var account = JSON.parse(d);
                var entitlements = await entitlementsController.getEntitlementsMap(token);

                if (account[constants.ISGROUP] === true)
                    return data;

                return res.status(200).json(methods.getJSONAccountObject(entitlements, account));

            }).on('error', (error) => {
                    logger.error("getAccount - " + error);
                    return res.status(400).json({"message": constants.WRONG_INPUT});
                }); 
        });
    
        req1.end();        
    } catch (error) {
        logger.error("getAccount - " + error);
        res.status(500).json({"message": error});
    }
}

methods.getJSONAccountObject = (entitlements, account) =>
{
    var appData={};
    appData[constants.ACCOUNT_ID] = account[constants.USERID];
    appData[constants.ACCOUNT_NAME] = account[constants.USERNAME];
    appData[constants.ACCOUNT_STATUS] = (account[constants.USERTYPEID] === 2)? constants.INACTIVE : constants.ACTIVE;
    appData[constants.IS_PRIVILEGED] = (account[constants.USERTYPEID] === 5) ;
    appData[constants.ROLE] = entitlements.get(account[constants.USERTYPEID]);
    appData[constants.EMAIL] = account[constants.EMAIL];

    const userObj = util.resolveFullName(account[constants.FULLNAME]);
    appData[constants.FIRST_NAME] = userObj[constants.FIRST_NAME];
    appData[constants.MIDDLE_NAME] = userObj[constants.MIDDLE_NAME];
    appData[constants.LAST_NAME] = userObj[constants.LAST_NAME];

    return appData;
}

methods.createAccount = (req, res) =>
{
    const token = jsonwebtoken.getTokenData(req);

    if (token == null || token.length === 0)
    {
        logger.error("createAccount - "+constants.TOKEN_ABSENT);
        return res.status(403).json({"message": constants.TOKEN_ABSENT});
    }

    const userName = util.constuctFullName(req.body.first_name, req.body.middle_name, req.body.last_name);

    const data = JSON.stringify({
        "userName": req.body.account_name,
        "userTypeId": req.body.entitlement_id,
        "email": req.body.email,
        "fullName": userName
      });

    const options = util.httpOption(token, 'POST', constants.ASE_CONSOLEUSERS, data.length);

    try {
        const req1 = https.request(options,  (res1) => {
            if (res1.statusCode !== 200)
            {
                logger.error("createAccount - " + res1.statusCode + " : "+res1.statusMessage);
                return res.status(res1.statusCode).json({"message": res1.statusMessage});
            }
            res1.on('data', async (d) => {
                var account = JSON.parse(d);
                var entitlements = await entitlementsController.getEntitlementsMap(token);
                return res.status(200).json(methods.getJSONAccountObject(entitlements, account));
            }).on('error', (error) => {
                    logger.error("createAccount - " + error);
                    return res.status(400).json({"message": constants.WRONG_INPUT});
                }); 
        });
        
        req1.write(data);
        req1.end();        
    } catch (error) {
        logger.error("createAccount - " + error);
        res.status(500).json({"message": error});
    }
}

methods.updateAccount = (req, res) =>
{
    const token = jsonwebtoken.getTokenData(req);

    if (token == null || token.length === 0)
    {
        logger.error("updateAccount - "+constants.TOKEN_ABSENT);
        return res.status(403).json({"message": constants.TOKEN_ABSENT});
    }

    const userName = util.constuctFullName(req.body.first_name, req.body.middle_name, req.body.last_name);

    const data = JSON.stringify({
        "userTypeId": req.body.entitlement_id,
        "email": req.body.email,
        "fullName": userName
      });

    const options = util.httpOption(token, 'PUT', constants.ASE_CONSOLEUSERS+req.params.accountid, data.length);

    try {
        const req1 = https.request(options,  (res1) => {
            if (res1.statusCode === 200)
                return res.status(200).json({"message": constants.SUCCESS});
            else
            {
                logger.error("updateAccount - " + res1.statusCode + " : "+res1.statusMessage);
                return res.status(res1.statusCode).json({"message": res1.statusMessage});
            }
        });
        
        req1.write(data);
        req1.end();        
    } catch (error) {
        logger.error("updateAccount - " + error);
        res.status(500).json({"message": error});
    }
}

methods.enableAccount = async (req, res) =>
{
    const token = jsonwebtoken.getTokenData(req);

    if (token == null || token.length === 0)
    {
        logger.error("enableAccount - "+constants.TOKEN_ABSENT);
        return res.status(403).json({"message": constants.TOKEN_ABSENT});
    }

    const data = JSON.stringify({
        "userTypeId": process.env.DEFAULT_USER_TYPE_ID,
      });

      const options = util.httpOption(token, 'PUT', constants.ASE_CONSOLEUSERS+req.params.accountid, data.length);

      try {
          const req1 = https.request(options,  (res1) => {
              if (res1.statusCode === 200)
                  return res.status(200).json({"message": constants.SUCCESS});
              else
              {
                  logger.error("enableAccount - " + res1.statusCode + " : "+res1.statusMessage);
                  return res.status(res1.statusCode).json({"message": res1.statusMessage});
              }
          });
          
          req1.write(data);
          req1.end();        
      } catch (error) {
          logger.error("enableAccount - " + error);
          res.status(500).json({"message": error});
      }      
}


methods.disableAccount = (req, res) =>
{
    const token = jsonwebtoken.getTokenData(req);

    if (token == null || token.length === 0)
    {
        logger.error("disableAccount - "+constants.TOKEN_ABSENT);
        return res.status(403).json({"message": constants.TOKEN_ABSENT});
    }

    const data = JSON.stringify({
        "userTypeId": 2,
      });

      const options = util.httpOption(token, 'PUT', constants.ASE_CONSOLEUSERS+req.params.accountid, data.length);

      try {
          const req1 = https.request(options,  (res1) => {
              if (res1.statusCode === 200)
                  return res.status(200).json({"message": constants.SUCCESS});
              else
              {
                  logger.error("disableAccount - " + res1.statusCode + " : "+res1.statusMessage);
                  return res.status(res1.statusCode).json({"message": res1.statusMessage});
              }
          });
          
          req1.write(data);
          req1.end();        
      } catch (error) {
          logger.error("disableAccount - " + error);
          res.status(500).json({"message": error});
      }   
}

methods.deleteAccount = (req, res) =>
{
    const token = jsonwebtoken.getTokenData(req);

    if (token == null || token.length === 0)
    {
        logger.error("deleteAccount - "+constants.TOKEN_ABSENT);
        return res.status(403).json({"message": constants.TOKEN_ABSENT});
    }

    const options = util.httpOption(token, 'DELETE', constants.ASE_CONSOLEUSERS+req.params.accountid);

    try {
        const req1 = https.request(options,  (res1) => {
            if (res1.statusCode === 200)
                return res.status(200).json({"message": constants.SUCCESS});
            else
            {
                logger.error("deleteAccount - " + res1.statusCode + " : "+res1.statusMessage);
                return res.status(res1.statusCode).json({"message": res1.statusMessage});
            }
        });
        
        req1.end();        
    } catch (error) {
        logger.error("deleteAccount - " + error);
        res.status(500).json({"message": error});
    }
}

module.exports = methods;