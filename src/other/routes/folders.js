const express = require('express');
const router = express.Router();
const foldersController = require('../controllers/foldersController');

/**
 * @swagger
 * /folders/create:
 *   put:
 *     summary: Creates a folder. 
 *     description: Creates a folder in the ASE system with the name of user.
 *     tags: 
 *       - folders
 *     parameters:
 *       - in: header
 *         name: auth-token
 *         required: true
 *         description: Provide the token returned by /login API in the format "bearer auth-token"
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               folderName:
 *                 type: string
 *               parentFolderId:
 *                 type: integer
 *     responses:
 *       201:
 *         description: Created
 *       400:
 *         description: Wrong input
 *       403:
 *         description: Invalid token or user does not exist.
 *       500:
 *         description: An unknown error has occured.
*/ 

router.put('/create', foldersController.createFolder);


/**
 * @swagger
 * /folders/{folderid}/scans:
 *   get:
 *     summary: List all the scans in the folder 
 *     description: Creates a folder in the ASE system with the name of user.
 *     tags: 
 *       - folders
 *     parameters:
 *       - in: header
 *         name: auth-token
 *         required: true
 *         description: Provide the token returned by /login API in the format "bearer auth-token"
 *         schema:
 *           type: string
 *       - in: path
 *         name: folderid
 *         required: true
 *         schema:
 *           type: integer 
 *     responses:
 *       200:
 *         description: Successful
 *       400:
 *         description: Wrong input
 *       403:
 *         description: Invalid token or user does not exist.
 *       500:
 *         description: An unknown error has occured.
*/ 

router.get('/:folderid/scans', foldersController.listScans);

module.exports = router;    