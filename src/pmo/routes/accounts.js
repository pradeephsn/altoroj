const express = require('express');
const router = express.Router();
const accountsController = require('../controllers/accountsController');
const validationMsgs = require('../../middleware/validate-request-schema');
const schemas = require('../../validation-schema/validationSchemas');
/**
 * @swagger
 * /accounts:
 *   get:
 *     summary: List all accounts. 
 *     description: List all accounts present in the AppScan system.
 *     tags: 
 *       - accounts
 *     parameters:
 *       - in: header
 *         name: auth-token
 *         required: true
 *         description: Provide the token returned by /login API in the format "bearer auth-token"
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Successful
 *       400:
 *         description: Wrong input
 *       403:
 *         description: Invalid token or user does not exist.
 *       500:
 *         description: An unknown error has occured.
*/ 

router.get('/', accountsController.getAllAccounts);

/**
 * @swagger
 * /accounts/{accountid}:
 *   get:
 *     summary: Get account information. 
 *     description: Get account information of the specified accountId.
 *     tags: 
 *       - accounts
 *     parameters:
 *       - in: header
 *         name: auth-token
 *         required: true
 *         description: Provide the token returned by /login API in the format "bearer auth-token"
 *         schema:
 *           type: string
 *       - in: path
 *         name: accountid
 *         required: true
 *         schema:
 *           type: integer 
 *     responses:
 *       200:
 *         description: Successful
 *       400:
 *         description: Wrong input
 *       403:
 *         description: Invalid token or user does not exist.
 *       500:
 *         description: An unknown error has occured.
*/ 

router.get('/:accountid', schemas.accountid, validationMsgs.validateRequestSchema, accountsController.getAccount);


/**
 * @swagger
 * /accounts:
 *   put:
 *     summary: Create an account. 
 *     description: Create an account with the details provided.
 *     tags: 
 *       - accounts
 *     parameters:
 *       - in: header
 *         name: auth-token
 *         required: true
 *         description: Provide the token returned by /login API in the format "bearer auth-token"
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               account_name:
 *                 type: string
 *                 required: true 
 *               entitlement_id:
 *                 type: integer
 *                 required: true 
 *               email:
 *                 type: string               
 *               first_name:
 *                 type: string
 *               middle_name:
 *                 type: string
 *               last_name:
 *                 type: string
 *                 required: true 
 *     responses:
 *       200:
 *         description: Successful
 *       400:
 *         description: Wrong input
 *       403:
 *         description: Invalid token or user does not exist.
 *       500:
 *         description: An unknown error has occured.
*/ 

router.put('/', schemas.accountCreate, validationMsgs.validateRequestSchema, accountsController.createAccount);


/**
 * @swagger
 * /accounts/{accountid}/update:
 *   put:
 *     summary: Update an account. 
 *     description: Update an account with the details provided.
 *     tags: 
 *       - accounts
 *     parameters:
 *       - in: header
 *         name: auth-token
 *         required: true
 *         description: Provide the token returned by /login API in the format "bearer auth-token"
 *         schema:
 *           type: string
 *       - in: path
 *         name: accountid
 *         required: true
 *         schema:
 *           type: integer 
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               entitlement_id:
 *                 type: integer
 *                 required: true 
 *               email:
 *                 type: string               
 *               first_name:
 *                 type: string
 *               middle_name:
 *                 type: string
 *               last_name:
 *                 type: string
 *                 required: true 
 *     responses:
 *       200:
 *         description: Successful
 *       400:
 *         description: Wrong input
 *       403:
 *         description: Invalid token or user does not exist.
 *       500:
 *         description: An unknown error has occured.
*/ 

router.put('/:accountid/update', schemas.accountUpdate, validationMsgs.validateRequestSchema, accountsController.updateAccount);


/**
 * @swagger
 * /accounts/{accountid}/enable:
 *   put:
 *     summary: Enable an account. 
 *     description: Enable an account.
 *     tags: 
 *       - accounts
 *     parameters:
 *       - in: header
 *         name: auth-token
 *         required: true
 *         description: Provide the token returned by /login API in the format "bearer auth-token"
 *         schema:
 *           type: string
 *       - in: path
 *         name: accountid
 *         required: true
 *         schema:
 *           type: integer 
 *     responses:
 *       200:
 *         description: Successful
 *       400:
 *         description: Wrong input
 *       403:
 *         description: Invalid token or user does not exist.
 *       500:
 *         description: An unknown error has occured.
*/ 

router.put('/:accountid/enable', schemas.accountid, validationMsgs.validateRequestSchema, accountsController.enableAccount);


/**
 * @swagger
 * /accounts/{accountid}/disable:
 *   put:
 *     summary: Disable an account. 
 *     description: Disable an account.
 *     tags: 
 *       - accounts
 *     parameters:
 *       - in: header
 *         name: auth-token
 *         required: true
 *         description: Provide the token returned by /login API in the format "bearer auth-token"
 *         schema:
 *           type: string
 *       - in: path
 *         name: accountid
 *         required: true
 *         schema:
 *           type: integer 
 *     responses:
 *       200:
 *         description: Successful
 *       400:
 *         description: Wrong input
 *       403:
 *         description: Invalid token or user does not exist.
 *       500:
 *         description: An unknown error has occured.
*/ 

router.put('/:accountid/disable', schemas.accountid, validationMsgs.validateRequestSchema, accountsController.disableAccount);


/**
 * @swagger
 * /accounts/{accountid}:
 *   delete:
 *     summary: Delete an account. 
 *     description: Delete an account.
 *     tags: 
 *       - accounts
 *     parameters:
 *       - in: header
 *         name: auth-token
 *         required: true
 *         description: Provide the token returned by /login API in the format "bearer auth-token"
 *         schema:
 *           type: string
 *       - in: path
 *         name: accountid
 *         required: true
 *         schema:
 *           type: integer 
 *     responses:
 *       200:
 *         description: Successful
 *       400:
 *         description: Wrong input
 *       403:
 *         description: Invalid token or user does not exist.
 *       500:
 *         description: An unknown error has occured.
*/ 

router.delete('/:accountid', schemas.accountid, validationMsgs.validateRequestSchema, accountsController.deleteAccount);


module.exports = router;    