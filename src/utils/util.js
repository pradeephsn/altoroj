const constants = require('./constants');

var methods = {};
methods.resolveFullName = function(fullName)
{
    var userObject = {};
    userObject[constants.LAST_NAME] = "";
    userObject[constants.FIRST_NAME] = "";
    userObject[constants.MIDDLE_NAME] = "";

    if(fullName.includes(", "))
    {
        const array1 = fullName.split(", ");
        userObject[constants.LAST_NAME] = array1[0];
        
        const array2 = array1[1].split(" ");
        userObject[constants.FIRST_NAME] = array2[0];    
        userObject[constants.MIDDLE_NAME] = (typeof array2[1] != 'undefined')? array2[1] : "";
    }
    else
    {
        userObject[constants.LAST_NAME] = fullName;    
    }

    return userObject;
}

methods.constuctFullName = (first_name, middle_name, last_name) => {

    var part2 = "";
    if (typeof middle_name != 'undefined' && middle_name.length >0) {
        part2 = ", "+middle_name;
        if ((typeof first_name != 'undefined' && first_name.length >0)) {
            part2 = part2 + " "+first_name;
        }
    } 

    return last_name+part2;
}

methods.httpOption = function(token, method, context, dataLength)
{
    return {
        hostname: process.env.ASE_HOSTNAME,
        port: process.env.ASE_PORT,
        path: '/'+process.env.ASE_CONTEXT+context,
        rejectUnauthorized: (process.env.REJECT_UNAUTHORIZAED===true),
        method: method,
        headers: {
          'Content-Type': 'application/json',
          'Content-Length': typeof dataLength==='undefined' ? 0 : dataLength,
          'Cookie': 'asc_session_id='+token,
          'asc_xsrf_token': token
        }
    };
}

module.exports = methods;