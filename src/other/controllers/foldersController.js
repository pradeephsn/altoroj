const log4js = require("log4js");
const logger = log4js.getLogger("foldersController");
const jsonwebtoken = require('../../utils/jsonwebtoken');
const https = require('https');
const util = require('../../utils/util')

var methods = {};

methods.createFolder = (req, res) =>
{
    const token = jsonwebtoken.getTokenData(req);

    if (token == null || token.length === 0)
    {
        logger.error("createFolder -  Token does not exist or invalid token");
        return res.status(403).json({"message": "Token does not exist or invalid token"});
    }

    const data = JSON.stringify({
        "parentId": req.body.parentFolderId,
        "folderName": req.body.folderName,
        "description": "",
        "contact": 0
      });

    const options = util.httpOption(token, 'POST', '/api/folders/create?responseFormat=json', data.length);

    const req1 = https.request(options, (res1) => {
        if (res1.statusCode === 200)
        {
            res1.on('data', function(d) {
                return res.status(201).json({"folderId":JSON.parse(d).folder.id})
            }).on('error', (error) => {
                    logger.error("createFolder - Folder creation has failed: " + error);
                    return res.status(400).json({"message": "Wrong input = "+error});
                }); 
        }
        else
        {
            return res.status(res1.statusCode).json({"message": res1.statusMessage});
        }

    });
    
    req1.write(data);
    req1.end();
}


methods.listScans = (req, res) =>
{
    const token = jsonwebtoken.getTokenData(req);

    if (token == null || token.length === 0)
    {
        logger.error("listScans -  Token does not exist or invalid token");
        return res.status(403).json({"message": "Token does not exist or invalid token"});
    }

    const options = util.httpOption(token, 'GET', '/api/folders/'+req.params.folderid+'/folderitems');

    const req1 = https.request(options, (res1) => {
        if (res1.statusCode !== 200)
        {
            logger.error("listScans -  failed with error code : " + res1.statusCode + " : "+res1.statusMessage);
            return res.status(res1.statusCode).json({"message": res1.statusMessage});
        }

        res1.on('data', function(d) {
            var data = [];
            var appData={};
            var jobs = JSON.parse(d)["folder-items"]["content-scan-job"];
            jobs.forEach(job => {
                appData["id"] = job["id"];
                appData["name"] = job["name"];
                appData["date-created"] = job["date-created"];
                appData["last-run"] = job["last-run"];

                if(typeof job["application"] !== 'undefined')
                    appData["app-id"] = job["application"]["app-id"];

                if(typeof job["state"] !== 'undefined')
                    appData["state"] = job["state"]["name"];
                
                data.push(appData);
                appData={};                        
            });

            return res.status(200).json(data);
        }).on('error', (error) => {
                logger.error("listscans -  failed: " + error);
                return res.status(400).json({"message": "Wrong input = "+error});
            }); 
    });

    req1.end();
}


module.exports = methods;